File Duplicate Finder
======================================

### Table of Contents
1. [Purpose](https://gitlab.com/jeremymreed/file-duplicate-finder#purpose)
2. [Usage](https://gitlab.com/jeremymreed/file-duplicate-finder#usage)
3. [License](https://gitlab.com/jeremymreed/file-duplicate-finder#license)


# Purpose:
This python program looks for duplicate files, and prints those files to screen.

Each group is identified by their sha256 hash value.

Output group format:
- (sha256 hash value) (same for each dupe)
- (Full path to file1) (file size)
- (Full path to file2) (file size)

Concrete sample output group:
```
f71b2580f16509232448e9f6671a579adabe7c811c260997b7e9ba24ea10cdb0
/home/jeremyr/Pictures/Wallpapers/wallhaven-662362-1920x1080.png, 2417337
/home/jeremyr/Pictures/Wallpapers/retrowave-sunset-1920x1080.png, 2417337
```

Full output will have between 0 and n groups.

# Usage:
This is meant to be installed via pip.  This program is not yet available via PyPI yet.
To install:
```
git clone https://gitlab.com/jeremymreed/file-duplicate-finder
cd file-duplicate-finder
pip install --user ./
```

```
$ lsdupes --input ./
```

Help message:
```
usage: lsdupes [-h] [-i [PATHS [PATHS ...]]] [--version] [-R] [-v]

optional arguments:
  -h, --help            show this help message and exit
  -i [PATHS [PATHS ...]], --input [PATHS [PATHS ...]]
                        paths to files or directories to be processed.
  --version             Output the version of this software and exit.
  -R, --recursive       Recursively process files in a directory. Ignored when
                        input is a file
  -v, --verbose         Increase output detail. -v for basic output, -vv for
                        detailed output.
```

# License:
This program is licensed under the GPLv2 License.
