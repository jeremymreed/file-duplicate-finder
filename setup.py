from setuptools import (setup, find_packages)

setup(
    name='file-duplicate-finder',
    version='0.5.0',
    description='Finds duplicate files.',
    author='Jeremy M. Reed',
    author_email='reeje76@gmail.com',
    license='GPLv2',
    url='https://gitlab.com/jeremymreed/file-duplicate-finder',
    packages=find_packages(),
    entry_points={
        'console_scripts': ['lsdupes=file_duplicate_finder.__main__:main']
    }
)
