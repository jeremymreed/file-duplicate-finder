#   Copyright (C) 2019 Jeremy M. Reed
#   
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#   
#   This program is distributed in the hope that it will be useful, but
#   WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
#   or FITNESS FOR A PARTICULAR PURPOSE.
#   See the GNU General Public License for more details.
#   
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
#

import json
import os
import logging

class IgnoreProcessor:
    def __init__(self):
        self.ignore_dirs = list()
        self.logger = logging.getLogger('lsdupes')

    def load_ignore_file(self, ignore_file_path):
        if ignore_file_path is not None:
            try:
                with open(ignore_file_path) as json_data:
                    raw_data = json.load(json_data)
                    for item in raw_data['dirs']:
                        self.ignore_dirs.append(item)
            except FileNotFoundError:
                self.logger.info('The given ignore file does not exist, defaulting to empty ignore list')

    # left is relative path to dir to ignore.
    # right is dir we are testing to see if we should ignore.
    def _test_relative_path(self, left, right):
        left_tokens = os.path.split(left)
        right_tokens = os.path.split(right)

        return left_tokens[len(left_tokens) - 1] == right_tokens[len(right_tokens) - 1]

    # This case is pretty straightforward.
    def _test_absolute_path(self, left, right):
        return os.path.abspath(os.path.expanduser(left)) == right

    # In the context of this program, a relative path matches only the current directory.
    # We will indicate relative paths by using '*/' to indicate relative paths.
    # This will match any subdirectory with the same name.
    # e.g. */foo matches /bar/foo and /some/tree/foo
    def _is_relative_path(self, input_path):
        path_tokens = os.path.split(input_path)
        return path_tokens[0] == '*'

    # input_path cannot be None, and must be a valid directory.
    def should_ignore(self, input_path):
        self.logger.debug('should we ignore ' + input_path + '?')
        for dir in self.ignore_dirs:
            if self._is_relative_path(dir):
                self.logger.debug(dir + ' is a relative directory')
                if self._test_relative_path(dir, input_path):
                    self.logger.debug('Yes we should ignore ' + input_path)
                    return True
            else:
                self.logger.debug(dir + ' is an absolute directory')
                if self._test_absolute_path(dir, input_path):
                    self.logger.debug('Yes we should ignore ' + input_path)
                    return True
        self.logger.debug('No we should NOT ignore ' + input_path)
        return False

    def print_data(self):
        print('Printing ignore list.')
        print('Items in ignore list: ' + str(len(self.ignore_dirs)))
        for item in self.ignore_dirs:
            print(item)
