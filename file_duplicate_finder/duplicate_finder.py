#   Copyright (C) 2019 Jeremy M. Reed
#   
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#   
#   This program is distributed in the hope that it will be useful, but
#   WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
#   or FITNESS FOR A PARTICULAR PURPOSE.
#   See the GNU General Public License for more details.
#   
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
#


import logging
import os
import shutil
import re
import hashlib
from datetime import datetime, timedelta

from .file_utils import FileUtils
from .result_data import ResultData
from .ignore_processor import IgnoreProcessor

"""
This module processes directories and files to look for duplicate files.
We are not going to follow links at the moment.
"""

class DuplicateFinder:
    def __init__(self, app_config):
        self.config = app_config
        logging.basicConfig()
        self.logger = logging.getLogger('lsdupes')
        if self.config.verbose == 0:
            logging.root.setLevel(logging.ERROR)
        else:
            logging.root.setLevel(logging.DEBUG)
        self.results = dict()
        self.total_files = 0
        self.total_dupes = 0
        self.total_size = 0
        self.total_dupe_size = 0
        self.ignore_processor = IgnoreProcessor()

    def print_results(self):
        for hash, result_list in self.results.items():
            # print(hash, file_list)
            if len(result_list) > 1:
                print(hash)
                for result in result_list:
                    print(result.file_path + ', ' + str(result.file_size))
                print()
        if self.config.stats:
            print('Total files processed: ' + str(self.total_files))
            print('Duplicate files found: ' + str(self.total_dupes))
            print('Size of files processed:           ' + str(self.total_size))
            print('Size of duplicate files processed: ' + str(self.total_dupe_size))
            print()

    def postprocess(self):
        for _, result_list in self.results.items():
            for result in result_list:
                self.total_files += len(result_list)
                self.total_size += result.file_size
                if len(result_list) > 1:
                    self.total_dupes += len(result_list)
                    self.total_dupe_size += result.file_size

    def process_result(self, hash, file_path, file_size):
        resultData = ResultData(file_path, file_size)
        if hash in self.results:
            self.results[hash].append(resultData)
        else:
            self.results[hash] = list()
            self.results[hash].append(resultData)

    def process_file(self, file_path, base=''):
        sha256_hash = hashlib.sha256()
        with open(file_path, "rb") as input:
            for byte_block in iter(lambda: input.read(4096), b""):
                sha256_hash.update(byte_block)
        hash = sha256_hash.hexdigest()
        file_size = os.path.getsize(file_path)
        self.process_result(hash, file_path, file_size)

    def process_directory(self, path, base=''):
        if not self.ignore_processor.should_ignore(path):
            items = os.listdir(path)
            for item in items:
                file_path = os.path.join(path, item)
                if os.path.isfile(file_path):
                    self.process_file(file_path, base)
                elif os.path.isdir(file_path):
                    if self.config.recursive:
                        self.process_directory(os.path.join(path, item), os.path.join(base, item))

    def run(self):
        """
        Runs the name_fixer module.

        :param config:  Application configuration.  AppConfig value.
        :returns nothing:
        """
        # check config.version first, before doing anything else.
        if self.config.version:
            print('Version: 0.4.0')
            exit(0)
        # config.path is only required if config.version is not set.
        if self.config.get_paths() is None:
            raise ValueError('The input path is required!')
        start_time = datetime.utcnow()

        if self.config.stats:
            print('Started at: ' + str(start_time) + '\n')

        print('Processing, please wait.')

        self.logger.debug("self.config.ignore_file_path: " + str(self.config.ignore_file_path))
        self.ignore_processor.load_ignore_file(self.config.ignore_file_path)

        for path in self.config.get_paths():
            path = FileUtils.process_raw_file_path(path)
            if os.path.exists(path):
                if os.path.isfile(path):
                    self.process_file(path)
                elif os.path.isdir(path):
                    self.process_directory(path)
                else:
                    raise ValueError('Got something other than regular file or directory.  Bailing!')
            else:
                raise ValueError('The input path does not exist')

        self.postprocess()
        self.print_results()
        end_time = datetime.utcnow()

        run_time = end_time - start_time

        if self.config.stats:
            print('Started at:     ' + str(start_time))
            print('Completed at:   ' + str(end_time))
            print('Total Run Time: ' + str(run_time))
