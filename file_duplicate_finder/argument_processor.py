#   Copyright (C) 2019 Jeremy M. Reed
#   
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#   
#   This program is distributed in the hope that it will be useful, but
#   WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
#   or FITNESS FOR A PARTICULAR PURPOSE.
#   See the GNU General Public License for more details.
#   
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
#


import argparse
from .app_config import AppConfig

"""
This module handles command line argument processing.
"""


class ArgumentProcessor:
    """
    This class handles command line argument processing.
    """
    def __init__(self):
        self.arg_parser = argparse.ArgumentParser()

    def process_args(self):
        """
        Process command line args, and construct an AppConfig object.
        :returns:  An instance of AppConfig.  This object describes the application's configuration based on
                   command line arguments.
        """
        self.arg_parser.add_argument('-i',
                                     '--input',
                                     type=str,
                                     dest='paths',
                                     help='paths to files or directories to be processed.',
                                     nargs='*')
        self.arg_parser.add_argument('--version',
                                     action='store_true',
                                     dest='version',
                                     help='Output the version of this software and exit.')
        self.arg_parser.add_argument('-R', '--recursive',
                                     action='store_true',
                                     dest='recursive',
                                     help='Recursively process files in a directory.\
                                     Ignored when input is a file')
        self.arg_parser.add_argument('--ignore-file',
                                     type=str,
                                     dest='ignore_file_path',
                                     help='Path to ignore file.')
        self.arg_parser.add_argument('-s', '--stats',
                                     action='store_true',
                                     dest='stats',
                                     help='Display statistics.  (number of files processed, run time.)')
        self.arg_parser.add_argument('-v', '--verbose',
                                     action='count',
                                     dest='verbose',
                                     default=0,
                                     help='Increase output detail. -v for basic output, -vv for detailed output.')

        args = self.arg_parser.parse_args()
        return AppConfig(args)
