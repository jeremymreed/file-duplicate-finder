#   Copyright (C) 2019 Jeremy M. Reed
#   
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#   
#   This program is distributed in the hope that it will be useful, but
#   WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
#   or FITNESS FOR A PARTICULAR PURPOSE.
#   See the GNU General Public License for more details.
#   
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
#


from file_duplicate_finder.duplicate_finder import DuplicateFinder
from .argument_processor import ArgumentProcessor


def main():
    """The main routine."""

    arg_processor = ArgumentProcessor()
    app_config = arg_processor.process_args()
    duplicate_finder = DuplicateFinder(app_config)
    duplicate_finder.run()


if __name__ == '__main__':
    main()
