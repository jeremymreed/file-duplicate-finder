#   Copyright (C) 2019 Jeremy M. Reed
#   
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#   
#   This program is distributed in the hope that it will be useful, but
#   WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
#   or FITNESS FOR A PARTICULAR PURPOSE.
#   See the GNU General Public License for more details.
#   
#   You should have received a copy of the GNU General Public License along
#   with this program; if not, write to the Free Software Foundation, Inc.,
#   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
#


"""
This module handles the application's configuration.
"""


class AppConfig:
    """
    This class encapsulates the application's configuration.

    :member dry_run: The application should run in dry run mode.  No destructive changes should be done.  Boolean value.
    :member should_move_file: The application should move the file, rather than copy it.  Boolean value.
    :member verbose: The application should run in verbose mode.  Output should include image_detail dump.
                     Boolean value.
    :member paths: Input file paths.  List of strings.  List value.
    :member output_dir: The application should copy/move output files to this directory.  If it does not exist, use the
                        current working directory.  String value.
    """
    def __init__(self, args):
        """
         AppConfig constructor.  Initialize this object with parsed command line arguments.

         :param args: Parsed command line arguments.
         :returns nothing:
         """
        self.version = args.version
        self.recursive = args.recursive
        self.stats = args.stats
        self.ignore_file_path = args.ignore_file_path
        self.verbose = args.verbose
        self.paths = args.paths

    def get_paths(self):
        """ :returns path: """
        return self.paths
